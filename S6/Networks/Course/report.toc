\babel@toc {russian}{}
\contentsline {chapter}{\numberline {1}Введение}{5}{chapter.1}% 
\contentsline {chapter}{\numberline {2}Описание организации}{6}{chapter.2}% 
\contentsline {section}{\numberline {2.1}Основная информация}{6}{section.2.1}% 
\contentsline {section}{\numberline {2.2}Структура организации}{6}{section.2.2}% 
\contentsline {chapter}{\numberline {3}Разработка транспортной подсистемы локальной вычислительной сети}{7}{chapter.3}% 
\contentsline {section}{\numberline {3.1}Схема предприятия}{7}{section.3.1}% 
\contentsline {section}{\numberline {3.2}Требования к сети}{7}{section.3.2}% 
\contentsline {section}{\numberline {3.3}Выбор топологии сети}{7}{section.3.3}% 
\contentsline {section}{\numberline {3.4}Выбор оборудования}{7}{section.3.4}% 
\contentsline {section}{\numberline {3.5}Адресация в ЛВС}{8}{section.3.5}% 
\contentsline {chapter}{\numberline {4}Расчет стоимости и эксплуатации локальной вычислительной сети}{10}{chapter.4}% 
\contentsline {chapter}{\numberline {5}Заключение}{11}{chapter.5}% 
\contentsline {chapter}{\numberline {6}Список использованных источников}{12}{chapter.6}% 
\contentsline {chapter}{\numberline {A}Схема предприятия}{13}{appendix.A}% 
\contentsline {chapter}{\numberline {B}Служба каталогов}{14}{appendix.B}% 
