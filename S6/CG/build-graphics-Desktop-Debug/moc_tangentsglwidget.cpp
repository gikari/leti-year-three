/****************************************************************************
** Meta object code from reading C++ file 'tangentsglwidget.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../graphics/glwidgets/tangentsglwidget.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'tangentsglwidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_TangentsGLWidget_t {
    QByteArrayData data[26];
    char stringdata0[560];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_TangentsGLWidget_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_TangentsGLWidget_t qt_meta_stringdata_TangentsGLWidget = {
    {
QT_MOC_LITERAL(0, 0, 16), // "TangentsGLWidget"
QT_MOC_LITERAL(1, 17, 30), // "inner_first_tangent_x1_changed"
QT_MOC_LITERAL(2, 48, 0), // ""
QT_MOC_LITERAL(3, 49, 30), // "inner_first_tangent_x2_changed"
QT_MOC_LITERAL(4, 80, 30), // "inner_first_tangent_y1_changed"
QT_MOC_LITERAL(5, 111, 30), // "inner_first_tangent_y2_changed"
QT_MOC_LITERAL(6, 142, 31), // "inner_second_tangent_x1_changed"
QT_MOC_LITERAL(7, 174, 31), // "inner_second_tangent_x2_changed"
QT_MOC_LITERAL(8, 206, 31), // "inner_second_tangent_y1_changed"
QT_MOC_LITERAL(9, 238, 31), // "inner_second_tangent_y2_changed"
QT_MOC_LITERAL(10, 270, 28), // "out_first_tangent_x1_changed"
QT_MOC_LITERAL(11, 299, 28), // "out_first_tangent_x2_changed"
QT_MOC_LITERAL(12, 328, 28), // "out_first_tangent_y1_changed"
QT_MOC_LITERAL(13, 357, 28), // "out_first_tangent_y2_changed"
QT_MOC_LITERAL(14, 386, 29), // "out_second_tangent_x1_changed"
QT_MOC_LITERAL(15, 416, 29), // "out_second_tangent_x2_changed"
QT_MOC_LITERAL(16, 446, 29), // "out_second_tangent_y1_changed"
QT_MOC_LITERAL(17, 476, 29), // "out_second_tangent_y2_changed"
QT_MOC_LITERAL(18, 506, 11), // "plotCircles"
QT_MOC_LITERAL(19, 518, 5), // "setX1"
QT_MOC_LITERAL(20, 524, 5), // "value"
QT_MOC_LITERAL(21, 530, 5), // "setY1"
QT_MOC_LITERAL(22, 536, 5), // "setR1"
QT_MOC_LITERAL(23, 542, 5), // "setX2"
QT_MOC_LITERAL(24, 548, 5), // "setY2"
QT_MOC_LITERAL(25, 554, 5) // "setR2"

    },
    "TangentsGLWidget\0inner_first_tangent_x1_changed\0"
    "\0inner_first_tangent_x2_changed\0"
    "inner_first_tangent_y1_changed\0"
    "inner_first_tangent_y2_changed\0"
    "inner_second_tangent_x1_changed\0"
    "inner_second_tangent_x2_changed\0"
    "inner_second_tangent_y1_changed\0"
    "inner_second_tangent_y2_changed\0"
    "out_first_tangent_x1_changed\0"
    "out_first_tangent_x2_changed\0"
    "out_first_tangent_y1_changed\0"
    "out_first_tangent_y2_changed\0"
    "out_second_tangent_x1_changed\0"
    "out_second_tangent_x2_changed\0"
    "out_second_tangent_y1_changed\0"
    "out_second_tangent_y2_changed\0plotCircles\0"
    "setX1\0value\0setY1\0setR1\0setX2\0setY2\0"
    "setR2"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_TangentsGLWidget[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      23,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      16,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,  129,    2, 0x06 /* Public */,
       3,    1,  132,    2, 0x06 /* Public */,
       4,    1,  135,    2, 0x06 /* Public */,
       5,    1,  138,    2, 0x06 /* Public */,
       6,    1,  141,    2, 0x06 /* Public */,
       7,    1,  144,    2, 0x06 /* Public */,
       8,    1,  147,    2, 0x06 /* Public */,
       9,    1,  150,    2, 0x06 /* Public */,
      10,    1,  153,    2, 0x06 /* Public */,
      11,    1,  156,    2, 0x06 /* Public */,
      12,    1,  159,    2, 0x06 /* Public */,
      13,    1,  162,    2, 0x06 /* Public */,
      14,    1,  165,    2, 0x06 /* Public */,
      15,    1,  168,    2, 0x06 /* Public */,
      16,    1,  171,    2, 0x06 /* Public */,
      17,    1,  174,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      18,    0,  177,    2, 0x0a /* Public */,
      19,    1,  178,    2, 0x0a /* Public */,
      21,    1,  181,    2, 0x0a /* Public */,
      22,    1,  184,    2, 0x0a /* Public */,
      23,    1,  187,    2, 0x0a /* Public */,
      24,    1,  190,    2, 0x0a /* Public */,
      25,    1,  193,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::Double,   20,
    QMetaType::Void, QMetaType::Double,   20,
    QMetaType::Void, QMetaType::Double,   20,
    QMetaType::Void, QMetaType::Double,   20,
    QMetaType::Void, QMetaType::Double,   20,
    QMetaType::Void, QMetaType::Double,   20,

       0        // eod
};

void TangentsGLWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<TangentsGLWidget *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->inner_first_tangent_x1_changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 1: _t->inner_first_tangent_x2_changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 2: _t->inner_first_tangent_y1_changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 3: _t->inner_first_tangent_y2_changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 4: _t->inner_second_tangent_x1_changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 5: _t->inner_second_tangent_x2_changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 6: _t->inner_second_tangent_y1_changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 7: _t->inner_second_tangent_y2_changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 8: _t->out_first_tangent_x1_changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 9: _t->out_first_tangent_x2_changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 10: _t->out_first_tangent_y1_changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 11: _t->out_first_tangent_y2_changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 12: _t->out_second_tangent_x1_changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 13: _t->out_second_tangent_x2_changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 14: _t->out_second_tangent_y1_changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 15: _t->out_second_tangent_y2_changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 16: _t->plotCircles(); break;
        case 17: _t->setX1((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 18: _t->setY1((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 19: _t->setR1((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 20: _t->setX2((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 21: _t->setY2((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 22: _t->setR2((*reinterpret_cast< double(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (TangentsGLWidget::*)(double );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TangentsGLWidget::inner_first_tangent_x1_changed)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (TangentsGLWidget::*)(double );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TangentsGLWidget::inner_first_tangent_x2_changed)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (TangentsGLWidget::*)(double );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TangentsGLWidget::inner_first_tangent_y1_changed)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (TangentsGLWidget::*)(double );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TangentsGLWidget::inner_first_tangent_y2_changed)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (TangentsGLWidget::*)(double );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TangentsGLWidget::inner_second_tangent_x1_changed)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (TangentsGLWidget::*)(double );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TangentsGLWidget::inner_second_tangent_x2_changed)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (TangentsGLWidget::*)(double );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TangentsGLWidget::inner_second_tangent_y1_changed)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (TangentsGLWidget::*)(double );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TangentsGLWidget::inner_second_tangent_y2_changed)) {
                *result = 7;
                return;
            }
        }
        {
            using _t = void (TangentsGLWidget::*)(double );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TangentsGLWidget::out_first_tangent_x1_changed)) {
                *result = 8;
                return;
            }
        }
        {
            using _t = void (TangentsGLWidget::*)(double );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TangentsGLWidget::out_first_tangent_x2_changed)) {
                *result = 9;
                return;
            }
        }
        {
            using _t = void (TangentsGLWidget::*)(double );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TangentsGLWidget::out_first_tangent_y1_changed)) {
                *result = 10;
                return;
            }
        }
        {
            using _t = void (TangentsGLWidget::*)(double );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TangentsGLWidget::out_first_tangent_y2_changed)) {
                *result = 11;
                return;
            }
        }
        {
            using _t = void (TangentsGLWidget::*)(double );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TangentsGLWidget::out_second_tangent_x1_changed)) {
                *result = 12;
                return;
            }
        }
        {
            using _t = void (TangentsGLWidget::*)(double );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TangentsGLWidget::out_second_tangent_x2_changed)) {
                *result = 13;
                return;
            }
        }
        {
            using _t = void (TangentsGLWidget::*)(double );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TangentsGLWidget::out_second_tangent_y1_changed)) {
                *result = 14;
                return;
            }
        }
        {
            using _t = void (TangentsGLWidget::*)(double );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TangentsGLWidget::out_second_tangent_y2_changed)) {
                *result = 15;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject TangentsGLWidget::staticMetaObject = { {
    &NicGLWidget::staticMetaObject,
    qt_meta_stringdata_TangentsGLWidget.data,
    qt_meta_data_TangentsGLWidget,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *TangentsGLWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *TangentsGLWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_TangentsGLWidget.stringdata0))
        return static_cast<void*>(this);
    return NicGLWidget::qt_metacast(_clname);
}

int TangentsGLWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = NicGLWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 23)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 23;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 23)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 23;
    }
    return _id;
}

// SIGNAL 0
void TangentsGLWidget::inner_first_tangent_x1_changed(double _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void TangentsGLWidget::inner_first_tangent_x2_changed(double _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void TangentsGLWidget::inner_first_tangent_y1_changed(double _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void TangentsGLWidget::inner_first_tangent_y2_changed(double _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void TangentsGLWidget::inner_second_tangent_x1_changed(double _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void TangentsGLWidget::inner_second_tangent_x2_changed(double _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void TangentsGLWidget::inner_second_tangent_y1_changed(double _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void TangentsGLWidget::inner_second_tangent_y2_changed(double _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void TangentsGLWidget::out_first_tangent_x1_changed(double _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}

// SIGNAL 9
void TangentsGLWidget::out_first_tangent_x2_changed(double _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 9, _a);
}

// SIGNAL 10
void TangentsGLWidget::out_first_tangent_y1_changed(double _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 10, _a);
}

// SIGNAL 11
void TangentsGLWidget::out_first_tangent_y2_changed(double _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 11, _a);
}

// SIGNAL 12
void TangentsGLWidget::out_second_tangent_x1_changed(double _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 12, _a);
}

// SIGNAL 13
void TangentsGLWidget::out_second_tangent_x2_changed(double _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 13, _a);
}

// SIGNAL 14
void TangentsGLWidget::out_second_tangent_y1_changed(double _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 14, _a);
}

// SIGNAL 15
void TangentsGLWidget::out_second_tangent_y2_changed(double _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 15, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
