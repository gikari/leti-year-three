/****************************************************************************
** Meta object code from reading C++ file 'surfacestab.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../graphics/tabs/surfacestab.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'surfacestab.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_SurfacesTab_t {
    QByteArrayData data[11];
    char stringdata0[129];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SurfacesTab_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SurfacesTab_t qt_meta_stringdata_SurfacesTab = {
    {
QT_MOC_LITERAL(0, 0, 11), // "SurfacesTab"
QT_MOC_LITERAL(1, 12, 13), // "updateVertexX"
QT_MOC_LITERAL(2, 26, 0), // ""
QT_MOC_LITERAL(3, 27, 5), // "value"
QT_MOC_LITERAL(4, 33, 13), // "vertexIndexes"
QT_MOC_LITERAL(5, 47, 13), // "updateVertexY"
QT_MOC_LITERAL(6, 61, 13), // "updateVertexZ"
QT_MOC_LITERAL(7, 75, 19), // "fillPolyhedronTable"
QT_MOC_LITERAL(8, 95, 22), // "updatePointCoordinates"
QT_MOC_LITERAL(9, 118, 3), // "row"
QT_MOC_LITERAL(10, 122, 6) // "column"

    },
    "SurfacesTab\0updateVertexX\0\0value\0"
    "vertexIndexes\0updateVertexY\0updateVertexZ\0"
    "fillPolyhedronTable\0updatePointCoordinates\0"
    "row\0column"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SurfacesTab[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    2,   54,    2, 0x06 /* Public */,
       5,    2,   59,    2, 0x06 /* Public */,
       6,    2,   64,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       7,    0,   69,    2, 0x0a /* Public */,
       8,    2,   70,    2, 0x0a /* Public */,
       1,    1,   75,    2, 0x0a /* Public */,
       5,    1,   78,    2, 0x0a /* Public */,
       6,    1,   81,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::Double, QMetaType::QPoint,    3,    4,
    QMetaType::Void, QMetaType::Double, QMetaType::QPoint,    3,    4,
    QMetaType::Void, QMetaType::Double, QMetaType::QPoint,    3,    4,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,    9,   10,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,

       0        // eod
};

void SurfacesTab::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<SurfacesTab *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->updateVertexX((*reinterpret_cast< double(*)>(_a[1])),(*reinterpret_cast< QPoint(*)>(_a[2]))); break;
        case 1: _t->updateVertexY((*reinterpret_cast< double(*)>(_a[1])),(*reinterpret_cast< QPoint(*)>(_a[2]))); break;
        case 2: _t->updateVertexZ((*reinterpret_cast< double(*)>(_a[1])),(*reinterpret_cast< QPoint(*)>(_a[2]))); break;
        case 3: _t->fillPolyhedronTable(); break;
        case 4: _t->updatePointCoordinates((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 5: _t->updateVertexX((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 6: _t->updateVertexY((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 7: _t->updateVertexZ((*reinterpret_cast< double(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (SurfacesTab::*)(double , QPoint );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&SurfacesTab::updateVertexX)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (SurfacesTab::*)(double , QPoint );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&SurfacesTab::updateVertexY)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (SurfacesTab::*)(double , QPoint );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&SurfacesTab::updateVertexZ)) {
                *result = 2;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject SurfacesTab::staticMetaObject = { {
    &QWidget::staticMetaObject,
    qt_meta_stringdata_SurfacesTab.data,
    qt_meta_data_SurfacesTab,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *SurfacesTab::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SurfacesTab::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_SurfacesTab.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int SurfacesTab::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 8;
    }
    return _id;
}

// SIGNAL 0
void SurfacesTab::updateVertexX(double _t1, QPoint _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void SurfacesTab::updateVertexY(double _t1, QPoint _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void SurfacesTab::updateVertexZ(double _t1, QPoint _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
