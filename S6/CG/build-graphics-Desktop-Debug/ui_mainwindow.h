/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "tabs/clippingtab.h"
#include "tabs/curvestab.h"
#include "tabs/lighttab.h"
#include "tabs/surfacestab.h"
#include "tabs/tangentstab.h"

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionPlot;
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    QTabWidget *tabWidget;
    TangentsTab *tangents_tab;
    CurvesTab *curves_tab;
    SurfacesTab *surfaces_tab;
    ClippingTab *clipping_tab;
    QWidget *visibility_tab;
    LightTab *light_tab;
    QMenuBar *menuBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->setWindowModality(Qt::NonModal);
        MainWindow->resize(1170, 590);
        MainWindow->setMinimumSize(QSize(1150, 590));
        MainWindow->setDocumentMode(false);
        MainWindow->setTabShape(QTabWidget::Rounded);
        actionPlot = new QAction(MainWindow);
        actionPlot->setObjectName(QString::fromUtf8("actionPlot"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tangents_tab = new TangentsTab();
        tangents_tab->setObjectName(QString::fromUtf8("tangents_tab"));
        tabWidget->addTab(tangents_tab, QString());
        curves_tab = new CurvesTab();
        curves_tab->setObjectName(QString::fromUtf8("curves_tab"));
        tabWidget->addTab(curves_tab, QString());
        surfaces_tab = new SurfacesTab();
        surfaces_tab->setObjectName(QString::fromUtf8("surfaces_tab"));
        tabWidget->addTab(surfaces_tab, QString());
        clipping_tab = new ClippingTab();
        clipping_tab->setObjectName(QString::fromUtf8("clipping_tab"));
        tabWidget->addTab(clipping_tab, QString());
        visibility_tab = new QWidget();
        visibility_tab->setObjectName(QString::fromUtf8("visibility_tab"));
        tabWidget->addTab(visibility_tab, QString());
        light_tab = new LightTab();
        light_tab->setObjectName(QString::fromUtf8("light_tab"));
        tabWidget->addTab(light_tab, QString());

        verticalLayout->addWidget(tabWidget);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1170, 28));
        MainWindow->setMenuBar(menuBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        tabWidget->setCurrentIndex(5);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "\320\232\320\276\320\274\320\277\321\214\321\216\321\202\320\265\321\200\320\275\320\260\321\217 \320\223\321\200\320\260\321\204\320\270\320\272\320\260", nullptr));
        actionPlot->setText(QApplication::translate("MainWindow", "Plot", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tangents_tab), QApplication::translate("MainWindow", "\320\236\320\272\321\200\321\203\320\266\320\275\320\276\321\201\321\202\320\270", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(curves_tab), QApplication::translate("MainWindow", "\320\232\321\200\320\270\320\262\321\213\320\265", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(surfaces_tab), QApplication::translate("MainWindow", "\320\237\320\276\320\262\320\265\321\200\321\205\320\275\320\276\321\201\321\202\320\270", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(clipping_tab), QApplication::translate("MainWindow", "\320\236\321\202\321\201\320\265\321\207\320\265\320\275\320\270\321\217", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(visibility_tab), QApplication::translate("MainWindow", "\320\222\320\270\320\264\320\270\320\274\320\276\321\201\321\202\321\214 \321\201\320\273\320\276\320\266\320\275\321\213\321\205 \321\201\321\206\320\265\320\275", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(light_tab), QApplication::translate("MainWindow", "\320\236\321\201\320\262\320\265\321\211\320\265\320\275\320\270\320\265", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
