/********************************************************************************
** Form generated from reading UI file 'lighttab.ui'
**
** Created by: Qt User Interface Compiler version 5.12.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LIGHTTAB_H
#define UI_LIGHTTAB_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "glwidgets/lightglwidget.h"

QT_BEGIN_NAMESPACE

class Ui_LightTab
{
public:
    QHBoxLayout *horizontalLayout;
    LightGLWidget *gl_widget;
    QWidget *sidebar;
    QVBoxLayout *vboxLayout;
    QGroupBox *observer_group;
    QVBoxLayout *verticalLayout;
    QDoubleSpinBox *observer_x;
    QDoubleSpinBox *observer_y;
    QDoubleSpinBox *observer_z;
    QDoubleSpinBox *observer_r;
    QSpacerItem *verticalSpacer;
    QGroupBox *lightsource_group;
    QVBoxLayout *verticalLayout_2;
    QDoubleSpinBox *lightsource_x;
    QDoubleSpinBox *lightsource_y;
    QDoubleSpinBox *lightsource_z;
    QDoubleSpinBox *lightsource_r;
    QDoubleSpinBox *lightsource_i;
    QSpacerItem *verticalSpacer_2;

    void setupUi(QWidget *LightTab)
    {
        if (LightTab->objectName().isEmpty())
            LightTab->setObjectName(QString::fromUtf8("LightTab"));
        LightTab->resize(829, 543);
        horizontalLayout = new QHBoxLayout(LightTab);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        gl_widget = new LightGLWidget(LightTab);
        gl_widget->setObjectName(QString::fromUtf8("gl_widget"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(4);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(gl_widget->sizePolicy().hasHeightForWidth());
        gl_widget->setSizePolicy(sizePolicy);

        horizontalLayout->addWidget(gl_widget);

        sidebar = new QWidget(LightTab);
        sidebar->setObjectName(QString::fromUtf8("sidebar"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(1);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(sidebar->sizePolicy().hasHeightForWidth());
        sidebar->setSizePolicy(sizePolicy1);
        vboxLayout = new QVBoxLayout(sidebar);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        observer_group = new QGroupBox(sidebar);
        observer_group->setObjectName(QString::fromUtf8("observer_group"));
        verticalLayout = new QVBoxLayout(observer_group);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        observer_x = new QDoubleSpinBox(observer_group);
        observer_x->setObjectName(QString::fromUtf8("observer_x"));
        observer_x->setMinimum(-360.000000000000000);
        observer_x->setMaximum(360.000000000000000);
        observer_x->setSingleStep(0.100000000000000);

        verticalLayout->addWidget(observer_x);

        observer_y = new QDoubleSpinBox(observer_group);
        observer_y->setObjectName(QString::fromUtf8("observer_y"));
        observer_y->setMinimum(-360.000000000000000);
        observer_y->setMaximum(360.000000000000000);
        observer_y->setSingleStep(0.100000000000000);

        verticalLayout->addWidget(observer_y);

        observer_z = new QDoubleSpinBox(observer_group);
        observer_z->setObjectName(QString::fromUtf8("observer_z"));
        observer_z->setMinimum(-360.000000000000000);
        observer_z->setMaximum(360.000000000000000);
        observer_z->setSingleStep(0.100000000000000);
        observer_z->setValue(5.000000000000000);

        verticalLayout->addWidget(observer_z);

        observer_r = new QDoubleSpinBox(observer_group);
        observer_r->setObjectName(QString::fromUtf8("observer_r"));
        observer_r->setMinimum(-360.000000000000000);
        observer_r->setMaximum(360.000000000000000);

        verticalLayout->addWidget(observer_r);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);


        vboxLayout->addWidget(observer_group);

        lightsource_group = new QGroupBox(sidebar);
        lightsource_group->setObjectName(QString::fromUtf8("lightsource_group"));
        verticalLayout_2 = new QVBoxLayout(lightsource_group);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        lightsource_x = new QDoubleSpinBox(lightsource_group);
        lightsource_x->setObjectName(QString::fromUtf8("lightsource_x"));
        lightsource_x->setMinimum(-99.989999999999995);
        lightsource_x->setSingleStep(0.100000000000000);
        lightsource_x->setValue(1.300000000000000);

        verticalLayout_2->addWidget(lightsource_x);

        lightsource_y = new QDoubleSpinBox(lightsource_group);
        lightsource_y->setObjectName(QString::fromUtf8("lightsource_y"));
        lightsource_y->setMinimum(-99.989999999999995);
        lightsource_y->setSingleStep(0.100000000000000);
        lightsource_y->setValue(1.300000000000000);

        verticalLayout_2->addWidget(lightsource_y);

        lightsource_z = new QDoubleSpinBox(lightsource_group);
        lightsource_z->setObjectName(QString::fromUtf8("lightsource_z"));
        lightsource_z->setMinimum(-99.989999999999995);
        lightsource_z->setSingleStep(0.100000000000000);
        lightsource_z->setValue(1.300000000000000);

        verticalLayout_2->addWidget(lightsource_z);

        lightsource_r = new QDoubleSpinBox(lightsource_group);
        lightsource_r->setObjectName(QString::fromUtf8("lightsource_r"));
        lightsource_r->setMinimum(-360.000000000000000);
        lightsource_r->setMaximum(360.000000000000000);

        verticalLayout_2->addWidget(lightsource_r);

        lightsource_i = new QDoubleSpinBox(lightsource_group);
        lightsource_i->setObjectName(QString::fromUtf8("lightsource_i"));
        lightsource_i->setMinimum(0.000000000000000);
        lightsource_i->setMaximum(1.000000000000000);
        lightsource_i->setSingleStep(0.010000000000000);
        lightsource_i->setValue(1.000000000000000);

        verticalLayout_2->addWidget(lightsource_i);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_2);


        vboxLayout->addWidget(lightsource_group);


        horizontalLayout->addWidget(sidebar);


        retranslateUi(LightTab);
        QObject::connect(lightsource_x, SIGNAL(valueChanged(double)), gl_widget, SLOT(setLightPositionX(double)));
        QObject::connect(lightsource_y, SIGNAL(valueChanged(double)), gl_widget, SLOT(setLightPositionY(double)));
        QObject::connect(lightsource_z, SIGNAL(valueChanged(double)), gl_widget, SLOT(setLightPositionZ(double)));
        QObject::connect(lightsource_r, SIGNAL(valueChanged(double)), gl_widget, SLOT(setLightRotationAngle(double)));
        QObject::connect(gl_widget, SIGNAL(lightPositionXUpdated(double)), lightsource_x, SLOT(setValue(double)));
        QObject::connect(gl_widget, SIGNAL(lightPositionYUpdated(double)), lightsource_y, SLOT(setValue(double)));
        QObject::connect(gl_widget, SIGNAL(lightPositionZUpdated(double)), lightsource_z, SLOT(setValue(double)));
        QObject::connect(observer_r, SIGNAL(valueChanged(double)), gl_widget, SLOT(setCameraRotationAngle(double)));
        QObject::connect(observer_z, SIGNAL(valueChanged(double)), gl_widget, SLOT(setCameraPositionZ(double)));
        QObject::connect(observer_y, SIGNAL(valueChanged(double)), gl_widget, SLOT(setCameraPositionY(double)));
        QObject::connect(observer_x, SIGNAL(valueChanged(double)), gl_widget, SLOT(setCameraPositionX(double)));
        QObject::connect(gl_widget, SIGNAL(cameraPositionZUpdated(double)), observer_z, SLOT(setValue(double)));
        QObject::connect(gl_widget, SIGNAL(cameraPositionYUpdated(double)), observer_y, SLOT(setValue(double)));
        QObject::connect(gl_widget, SIGNAL(cameraPositionXUpdated(double)), observer_x, SLOT(setValue(double)));
        QObject::connect(lightsource_i, SIGNAL(valueChanged(double)), gl_widget, SLOT(setLightIntensity(double)));

        QMetaObject::connectSlotsByName(LightTab);
    } // setupUi

    void retranslateUi(QWidget *LightTab)
    {
        LightTab->setWindowTitle(QApplication::translate("LightTab", "Form", nullptr));
        observer_group->setTitle(QApplication::translate("LightTab", "\320\235\320\260\320\261\320\273\321\216\320\264\320\260\321\202\320\265\320\273\321\214", nullptr));
        observer_x->setPrefix(QApplication::translate("LightTab", "X | ", nullptr));
        observer_y->setPrefix(QApplication::translate("LightTab", "Y | ", nullptr));
        observer_z->setPrefix(QApplication::translate("LightTab", "Z | ", nullptr));
        observer_r->setPrefix(QApplication::translate("LightTab", "R | ", nullptr));
        lightsource_group->setTitle(QApplication::translate("LightTab", "\320\230\321\201\321\202\320\276\321\207\320\275\320\270\320\272 \321\201\320\262\320\265\321\202\320\260", nullptr));
        lightsource_x->setPrefix(QApplication::translate("LightTab", "X | ", nullptr));
        lightsource_y->setPrefix(QApplication::translate("LightTab", "Y | ", nullptr));
        lightsource_z->setPrefix(QApplication::translate("LightTab", "Z | ", nullptr));
        lightsource_r->setPrefix(QApplication::translate("LightTab", "R | ", nullptr));
        lightsource_i->setPrefix(QApplication::translate("LightTab", "I | ", nullptr));
    } // retranslateUi

};

namespace Ui {
    class LightTab: public Ui_LightTab {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LIGHTTAB_H
