/****************************************************************************
** Meta object code from reading C++ file 'lightglwidget.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../graphics/glwidgets/lightglwidget.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'lightglwidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_LightGLWidget_t {
    QByteArrayData data[19];
    char stringdata0[336];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_LightGLWidget_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_LightGLWidget_t qt_meta_stringdata_LightGLWidget = {
    {
QT_MOC_LITERAL(0, 0, 13), // "LightGLWidget"
QT_MOC_LITERAL(1, 14, 21), // "lightPositionXUpdated"
QT_MOC_LITERAL(2, 36, 0), // ""
QT_MOC_LITERAL(3, 37, 5), // "value"
QT_MOC_LITERAL(4, 43, 21), // "lightPositionYUpdated"
QT_MOC_LITERAL(5, 65, 21), // "lightPositionZUpdated"
QT_MOC_LITERAL(6, 87, 22), // "cameraPositionXUpdated"
QT_MOC_LITERAL(7, 110, 22), // "cameraPositionYUpdated"
QT_MOC_LITERAL(8, 133, 22), // "cameraPositionZUpdated"
QT_MOC_LITERAL(9, 156, 18), // "setCameraPositionX"
QT_MOC_LITERAL(10, 175, 18), // "setCameraPositionY"
QT_MOC_LITERAL(11, 194, 18), // "setCameraPositionZ"
QT_MOC_LITERAL(12, 213, 22), // "setCameraRotationAngle"
QT_MOC_LITERAL(13, 236, 5), // "angle"
QT_MOC_LITERAL(14, 242, 17), // "setLightPositionX"
QT_MOC_LITERAL(15, 260, 17), // "setLightPositionY"
QT_MOC_LITERAL(16, 278, 17), // "setLightPositionZ"
QT_MOC_LITERAL(17, 296, 17), // "setLightIntensity"
QT_MOC_LITERAL(18, 314, 21) // "setLightRotationAngle"

    },
    "LightGLWidget\0lightPositionXUpdated\0"
    "\0value\0lightPositionYUpdated\0"
    "lightPositionZUpdated\0cameraPositionXUpdated\0"
    "cameraPositionYUpdated\0cameraPositionZUpdated\0"
    "setCameraPositionX\0setCameraPositionY\0"
    "setCameraPositionZ\0setCameraRotationAngle\0"
    "angle\0setLightPositionX\0setLightPositionY\0"
    "setLightPositionZ\0setLightIntensity\0"
    "setLightRotationAngle"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_LightGLWidget[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      15,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       6,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   89,    2, 0x06 /* Public */,
       4,    1,   92,    2, 0x06 /* Public */,
       5,    1,   95,    2, 0x06 /* Public */,
       6,    1,   98,    2, 0x06 /* Public */,
       7,    1,  101,    2, 0x06 /* Public */,
       8,    1,  104,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       9,    1,  107,    2, 0x0a /* Public */,
      10,    1,  110,    2, 0x0a /* Public */,
      11,    1,  113,    2, 0x0a /* Public */,
      12,    1,  116,    2, 0x0a /* Public */,
      14,    1,  119,    2, 0x0a /* Public */,
      15,    1,  122,    2, 0x0a /* Public */,
      16,    1,  125,    2, 0x0a /* Public */,
      17,    1,  128,    2, 0x0a /* Public */,
      18,    1,  131,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,

 // slots: parameters
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,   13,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,   13,

       0        // eod
};

void LightGLWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<LightGLWidget *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->lightPositionXUpdated((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 1: _t->lightPositionYUpdated((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 2: _t->lightPositionZUpdated((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 3: _t->cameraPositionXUpdated((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 4: _t->cameraPositionYUpdated((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 5: _t->cameraPositionZUpdated((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 6: _t->setCameraPositionX((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 7: _t->setCameraPositionY((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 8: _t->setCameraPositionZ((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 9: _t->setCameraRotationAngle((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 10: _t->setLightPositionX((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 11: _t->setLightPositionY((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 12: _t->setLightPositionZ((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 13: _t->setLightIntensity((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 14: _t->setLightRotationAngle((*reinterpret_cast< double(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (LightGLWidget::*)(double );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&LightGLWidget::lightPositionXUpdated)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (LightGLWidget::*)(double );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&LightGLWidget::lightPositionYUpdated)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (LightGLWidget::*)(double );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&LightGLWidget::lightPositionZUpdated)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (LightGLWidget::*)(double );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&LightGLWidget::cameraPositionXUpdated)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (LightGLWidget::*)(double );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&LightGLWidget::cameraPositionYUpdated)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (LightGLWidget::*)(double );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&LightGLWidget::cameraPositionZUpdated)) {
                *result = 5;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject LightGLWidget::staticMetaObject = { {
    &NicGLWidget::staticMetaObject,
    qt_meta_stringdata_LightGLWidget.data,
    qt_meta_data_LightGLWidget,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *LightGLWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *LightGLWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_LightGLWidget.stringdata0))
        return static_cast<void*>(this);
    return NicGLWidget::qt_metacast(_clname);
}

int LightGLWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = NicGLWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 15)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 15;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 15)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 15;
    }
    return _id;
}

// SIGNAL 0
void LightGLWidget::lightPositionXUpdated(double _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void LightGLWidget::lightPositionYUpdated(double _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void LightGLWidget::lightPositionZUpdated(double _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void LightGLWidget::cameraPositionXUpdated(double _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void LightGLWidget::cameraPositionYUpdated(double _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void LightGLWidget::cameraPositionZUpdated(double _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
