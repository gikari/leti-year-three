/****************************************************************************
** Meta object code from reading C++ file 'tangentstab.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../graphics/tabs/tangentstab.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'tangentstab.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_TangentsTab_t {
    QByteArrayData data[19];
    char stringdata0[505];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_TangentsTab_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_TangentsTab_t qt_meta_stringdata_TangentsTab = {
    {
QT_MOC_LITERAL(0, 0, 11), // "TangentsTab"
QT_MOC_LITERAL(1, 12, 30), // "inner_first_tangent_x1_changed"
QT_MOC_LITERAL(2, 43, 0), // ""
QT_MOC_LITERAL(3, 44, 3), // "val"
QT_MOC_LITERAL(4, 48, 30), // "inner_first_tangent_x2_changed"
QT_MOC_LITERAL(5, 79, 30), // "inner_first_tangent_y1_changed"
QT_MOC_LITERAL(6, 110, 30), // "inner_first_tangent_y2_changed"
QT_MOC_LITERAL(7, 141, 31), // "inner_second_tangent_x1_changed"
QT_MOC_LITERAL(8, 173, 31), // "inner_second_tangent_x2_changed"
QT_MOC_LITERAL(9, 205, 31), // "inner_second_tangent_y1_changed"
QT_MOC_LITERAL(10, 237, 31), // "inner_second_tangent_y2_changed"
QT_MOC_LITERAL(11, 269, 28), // "out_first_tangent_x1_changed"
QT_MOC_LITERAL(12, 298, 28), // "out_first_tangent_x2_changed"
QT_MOC_LITERAL(13, 327, 28), // "out_first_tangent_y1_changed"
QT_MOC_LITERAL(14, 356, 28), // "out_first_tangent_y2_changed"
QT_MOC_LITERAL(15, 385, 29), // "out_second_tangent_x1_changed"
QT_MOC_LITERAL(16, 415, 29), // "out_second_tangent_x2_changed"
QT_MOC_LITERAL(17, 445, 29), // "out_second_tangent_y1_changed"
QT_MOC_LITERAL(18, 475, 29) // "out_second_tangent_y2_changed"

    },
    "TangentsTab\0inner_first_tangent_x1_changed\0"
    "\0val\0inner_first_tangent_x2_changed\0"
    "inner_first_tangent_y1_changed\0"
    "inner_first_tangent_y2_changed\0"
    "inner_second_tangent_x1_changed\0"
    "inner_second_tangent_x2_changed\0"
    "inner_second_tangent_y1_changed\0"
    "inner_second_tangent_y2_changed\0"
    "out_first_tangent_x1_changed\0"
    "out_first_tangent_x2_changed\0"
    "out_first_tangent_y1_changed\0"
    "out_first_tangent_y2_changed\0"
    "out_second_tangent_x1_changed\0"
    "out_second_tangent_x2_changed\0"
    "out_second_tangent_y1_changed\0"
    "out_second_tangent_y2_changed"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_TangentsTab[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      16,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   94,    2, 0x0a /* Public */,
       4,    1,   97,    2, 0x0a /* Public */,
       5,    1,  100,    2, 0x0a /* Public */,
       6,    1,  103,    2, 0x0a /* Public */,
       7,    1,  106,    2, 0x0a /* Public */,
       8,    1,  109,    2, 0x0a /* Public */,
       9,    1,  112,    2, 0x0a /* Public */,
      10,    1,  115,    2, 0x0a /* Public */,
      11,    1,  118,    2, 0x0a /* Public */,
      12,    1,  121,    2, 0x0a /* Public */,
      13,    1,  124,    2, 0x0a /* Public */,
      14,    1,  127,    2, 0x0a /* Public */,
      15,    1,  130,    2, 0x0a /* Public */,
      16,    1,  133,    2, 0x0a /* Public */,
      17,    1,  136,    2, 0x0a /* Public */,
      18,    1,  139,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,

       0        // eod
};

void TangentsTab::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<TangentsTab *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->inner_first_tangent_x1_changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 1: _t->inner_first_tangent_x2_changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 2: _t->inner_first_tangent_y1_changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 3: _t->inner_first_tangent_y2_changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 4: _t->inner_second_tangent_x1_changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 5: _t->inner_second_tangent_x2_changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 6: _t->inner_second_tangent_y1_changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 7: _t->inner_second_tangent_y2_changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 8: _t->out_first_tangent_x1_changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 9: _t->out_first_tangent_x2_changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 10: _t->out_first_tangent_y1_changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 11: _t->out_first_tangent_y2_changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 12: _t->out_second_tangent_x1_changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 13: _t->out_second_tangent_x2_changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 14: _t->out_second_tangent_y1_changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 15: _t->out_second_tangent_y2_changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject TangentsTab::staticMetaObject = { {
    &QWidget::staticMetaObject,
    qt_meta_stringdata_TangentsTab.data,
    qt_meta_data_TangentsTab,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *TangentsTab::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *TangentsTab::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_TangentsTab.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int TangentsTab::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 16)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 16;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 16)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 16;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
