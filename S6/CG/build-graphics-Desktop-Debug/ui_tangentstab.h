/********************************************************************************
** Form generated from reading UI file 'tangentstab.ui'
**
** Created by: Qt User Interface Compiler version 5.12.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TANGENTSTAB_H
#define UI_TANGENTSTAB_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "glwidgets/tangentsglwidget.h"

QT_BEGIN_NAMESPACE

class Ui_TangentsTab
{
public:
    QHBoxLayout *horizontalLayout;
    TangentsGLWidget *gl_widget;
    QWidget *sidebar;
    QVBoxLayout *verticalLayout_5;
    QWidget *circle_params;
    QHBoxLayout *horizontalLayout_2;
    QGroupBox *circle1_group;
    QVBoxLayout *verticalLayout;
    QDoubleSpinBox *x1_box;
    QDoubleSpinBox *y1_box;
    QDoubleSpinBox *r1_box;
    QSpacerItem *verticalSpacer;
    QGroupBox *circle2_group;
    QVBoxLayout *verticalLayout_2;
    QDoubleSpinBox *x2_box;
    QDoubleSpinBox *y2_box;
    QDoubleSpinBox *r2_box;
    QSpacerItem *verticalSpacer_2;
    QWidget *tangents_params;
    QHBoxLayout *horizontalLayout_3;
    QGroupBox *outer_tangent_group;
    QVBoxLayout *verticalLayout_3;
    QGroupBox *outer_tangent_1_group;
    QGridLayout *gridLayout;
    QLineEdit *xot1_1_le;
    QLineEdit *xot1_2_le;
    QLineEdit *yot1_1_le;
    QLineEdit *yot1_2_le;
    QGroupBox *outer_tangent_2_group;
    QGridLayout *gridLayout_3;
    QLineEdit *xot2_1_le;
    QLineEdit *xot2_2_le;
    QLineEdit *yot2_1_le;
    QLineEdit *yot2_2_le;
    QGroupBox *inner_tangent_group;
    QVBoxLayout *verticalLayout_4;
    QGroupBox *inner_tangent_1_group;
    QGridLayout *gridLayout_2;
    QLineEdit *xit1_1_le;
    QLineEdit *xit1_2_le;
    QLineEdit *yit1_1_le;
    QLineEdit *yit1_2_le;
    QGroupBox *inner_tangent_2_group;
    QGridLayout *gridLayout_4;
    QLineEdit *xit2_1_le;
    QLineEdit *xit2_2_le;
    QLineEdit *yit2_1_le;
    QLineEdit *yit2_2_le;

    void setupUi(QWidget *TangentsTab)
    {
        if (TangentsTab->objectName().isEmpty())
            TangentsTab->setObjectName(QString::fromUtf8("TangentsTab"));
        TangentsTab->resize(1217, 583);
        horizontalLayout = new QHBoxLayout(TangentsTab);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        gl_widget = new TangentsGLWidget(TangentsTab);
        gl_widget->setObjectName(QString::fromUtf8("gl_widget"));
        gl_widget->setMinimumSize(QSize(540, 540));

        horizontalLayout->addWidget(gl_widget);

        sidebar = new QWidget(TangentsTab);
        sidebar->setObjectName(QString::fromUtf8("sidebar"));
        QSizePolicy sizePolicy(QSizePolicy::Maximum, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(sidebar->sizePolicy().hasHeightForWidth());
        sidebar->setSizePolicy(sizePolicy);
        sidebar->setMaximumSize(QSize(500, 16777215));
        verticalLayout_5 = new QVBoxLayout(sidebar);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        circle_params = new QWidget(sidebar);
        circle_params->setObjectName(QString::fromUtf8("circle_params"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(2);
        sizePolicy1.setHeightForWidth(circle_params->sizePolicy().hasHeightForWidth());
        circle_params->setSizePolicy(sizePolicy1);
        horizontalLayout_2 = new QHBoxLayout(circle_params);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        circle1_group = new QGroupBox(circle_params);
        circle1_group->setObjectName(QString::fromUtf8("circle1_group"));
        verticalLayout = new QVBoxLayout(circle1_group);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        x1_box = new QDoubleSpinBox(circle1_group);
        x1_box->setObjectName(QString::fromUtf8("x1_box"));
        x1_box->setDecimals(4);
        x1_box->setMinimum(-99.989999999999995);
        x1_box->setSingleStep(0.010000000000000);
        x1_box->setValue(0.500000000000000);

        verticalLayout->addWidget(x1_box);

        y1_box = new QDoubleSpinBox(circle1_group);
        y1_box->setObjectName(QString::fromUtf8("y1_box"));
        y1_box->setDecimals(4);
        y1_box->setMinimum(-99.989999999999995);
        y1_box->setSingleStep(0.010000000000000);
        y1_box->setValue(0.500000000000000);

        verticalLayout->addWidget(y1_box);

        r1_box = new QDoubleSpinBox(circle1_group);
        r1_box->setObjectName(QString::fromUtf8("r1_box"));
        r1_box->setDecimals(4);
        r1_box->setSingleStep(0.010000000000000);
        r1_box->setValue(1.000000000000000);

        verticalLayout->addWidget(r1_box);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);


        horizontalLayout_2->addWidget(circle1_group);

        circle2_group = new QGroupBox(circle_params);
        circle2_group->setObjectName(QString::fromUtf8("circle2_group"));
        verticalLayout_2 = new QVBoxLayout(circle2_group);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        x2_box = new QDoubleSpinBox(circle2_group);
        x2_box->setObjectName(QString::fromUtf8("x2_box"));
        x2_box->setDecimals(4);
        x2_box->setMinimum(-99.989999999999995);
        x2_box->setSingleStep(0.010000000000000);
        x2_box->setValue(-1.000000000000000);

        verticalLayout_2->addWidget(x2_box);

        y2_box = new QDoubleSpinBox(circle2_group);
        y2_box->setObjectName(QString::fromUtf8("y2_box"));
        y2_box->setDecimals(4);
        y2_box->setMinimum(-99.989999999999995);
        y2_box->setSingleStep(0.010000000000000);
        y2_box->setValue(-1.000000000000000);

        verticalLayout_2->addWidget(y2_box);

        r2_box = new QDoubleSpinBox(circle2_group);
        r2_box->setObjectName(QString::fromUtf8("r2_box"));
        r2_box->setDecimals(4);
        r2_box->setSingleStep(0.010000000000000);
        r2_box->setValue(0.500000000000000);

        verticalLayout_2->addWidget(r2_box);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_2);


        horizontalLayout_2->addWidget(circle2_group);


        verticalLayout_5->addWidget(circle_params);

        tangents_params = new QWidget(sidebar);
        tangents_params->setObjectName(QString::fromUtf8("tangents_params"));
        QSizePolicy sizePolicy2(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(3);
        sizePolicy2.setHeightForWidth(tangents_params->sizePolicy().hasHeightForWidth());
        tangents_params->setSizePolicy(sizePolicy2);
        horizontalLayout_3 = new QHBoxLayout(tangents_params);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        outer_tangent_group = new QGroupBox(tangents_params);
        outer_tangent_group->setObjectName(QString::fromUtf8("outer_tangent_group"));
        verticalLayout_3 = new QVBoxLayout(outer_tangent_group);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        outer_tangent_1_group = new QGroupBox(outer_tangent_group);
        outer_tangent_1_group->setObjectName(QString::fromUtf8("outer_tangent_1_group"));
        gridLayout = new QGridLayout(outer_tangent_1_group);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        xot1_1_le = new QLineEdit(outer_tangent_1_group);
        xot1_1_le->setObjectName(QString::fromUtf8("xot1_1_le"));
        xot1_1_le->setReadOnly(true);

        gridLayout->addWidget(xot1_1_le, 0, 0, 1, 1);

        xot1_2_le = new QLineEdit(outer_tangent_1_group);
        xot1_2_le->setObjectName(QString::fromUtf8("xot1_2_le"));
        xot1_2_le->setReadOnly(true);

        gridLayout->addWidget(xot1_2_le, 0, 1, 1, 1);

        yot1_1_le = new QLineEdit(outer_tangent_1_group);
        yot1_1_le->setObjectName(QString::fromUtf8("yot1_1_le"));
        yot1_1_le->setReadOnly(true);

        gridLayout->addWidget(yot1_1_le, 1, 0, 1, 1);

        yot1_2_le = new QLineEdit(outer_tangent_1_group);
        yot1_2_le->setObjectName(QString::fromUtf8("yot1_2_le"));
        yot1_2_le->setReadOnly(true);

        gridLayout->addWidget(yot1_2_le, 1, 1, 1, 1);


        verticalLayout_3->addWidget(outer_tangent_1_group);

        outer_tangent_2_group = new QGroupBox(outer_tangent_group);
        outer_tangent_2_group->setObjectName(QString::fromUtf8("outer_tangent_2_group"));
        gridLayout_3 = new QGridLayout(outer_tangent_2_group);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        xot2_1_le = new QLineEdit(outer_tangent_2_group);
        xot2_1_le->setObjectName(QString::fromUtf8("xot2_1_le"));
        xot2_1_le->setReadOnly(true);

        gridLayout_3->addWidget(xot2_1_le, 0, 0, 1, 1);

        xot2_2_le = new QLineEdit(outer_tangent_2_group);
        xot2_2_le->setObjectName(QString::fromUtf8("xot2_2_le"));
        xot2_2_le->setReadOnly(true);

        gridLayout_3->addWidget(xot2_2_le, 0, 1, 1, 1);

        yot2_1_le = new QLineEdit(outer_tangent_2_group);
        yot2_1_le->setObjectName(QString::fromUtf8("yot2_1_le"));
        yot2_1_le->setReadOnly(true);

        gridLayout_3->addWidget(yot2_1_le, 1, 0, 1, 1);

        yot2_2_le = new QLineEdit(outer_tangent_2_group);
        yot2_2_le->setObjectName(QString::fromUtf8("yot2_2_le"));
        yot2_2_le->setReadOnly(true);

        gridLayout_3->addWidget(yot2_2_le, 1, 1, 1, 1);


        verticalLayout_3->addWidget(outer_tangent_2_group);


        horizontalLayout_3->addWidget(outer_tangent_group);

        inner_tangent_group = new QGroupBox(tangents_params);
        inner_tangent_group->setObjectName(QString::fromUtf8("inner_tangent_group"));
        QSizePolicy sizePolicy3(QSizePolicy::Preferred, QSizePolicy::Minimum);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(inner_tangent_group->sizePolicy().hasHeightForWidth());
        inner_tangent_group->setSizePolicy(sizePolicy3);
        verticalLayout_4 = new QVBoxLayout(inner_tangent_group);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        inner_tangent_1_group = new QGroupBox(inner_tangent_group);
        inner_tangent_1_group->setObjectName(QString::fromUtf8("inner_tangent_1_group"));
        gridLayout_2 = new QGridLayout(inner_tangent_1_group);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        xit1_1_le = new QLineEdit(inner_tangent_1_group);
        xit1_1_le->setObjectName(QString::fromUtf8("xit1_1_le"));
        xit1_1_le->setReadOnly(true);

        gridLayout_2->addWidget(xit1_1_le, 0, 0, 1, 1);

        xit1_2_le = new QLineEdit(inner_tangent_1_group);
        xit1_2_le->setObjectName(QString::fromUtf8("xit1_2_le"));
        xit1_2_le->setReadOnly(true);

        gridLayout_2->addWidget(xit1_2_le, 0, 1, 1, 1);

        yit1_1_le = new QLineEdit(inner_tangent_1_group);
        yit1_1_le->setObjectName(QString::fromUtf8("yit1_1_le"));
        yit1_1_le->setReadOnly(true);

        gridLayout_2->addWidget(yit1_1_le, 1, 0, 1, 1);

        yit1_2_le = new QLineEdit(inner_tangent_1_group);
        yit1_2_le->setObjectName(QString::fromUtf8("yit1_2_le"));
        yit1_2_le->setReadOnly(true);

        gridLayout_2->addWidget(yit1_2_le, 1, 1, 1, 1);


        verticalLayout_4->addWidget(inner_tangent_1_group);

        inner_tangent_2_group = new QGroupBox(inner_tangent_group);
        inner_tangent_2_group->setObjectName(QString::fromUtf8("inner_tangent_2_group"));
        gridLayout_4 = new QGridLayout(inner_tangent_2_group);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        xit2_1_le = new QLineEdit(inner_tangent_2_group);
        xit2_1_le->setObjectName(QString::fromUtf8("xit2_1_le"));
        xit2_1_le->setReadOnly(true);

        gridLayout_4->addWidget(xit2_1_le, 0, 0, 1, 1);

        xit2_2_le = new QLineEdit(inner_tangent_2_group);
        xit2_2_le->setObjectName(QString::fromUtf8("xit2_2_le"));
        xit2_2_le->setReadOnly(true);

        gridLayout_4->addWidget(xit2_2_le, 0, 1, 1, 1);

        yit2_1_le = new QLineEdit(inner_tangent_2_group);
        yit2_1_le->setObjectName(QString::fromUtf8("yit2_1_le"));
        yit2_1_le->setReadOnly(true);

        gridLayout_4->addWidget(yit2_1_le, 1, 0, 1, 1);

        yit2_2_le = new QLineEdit(inner_tangent_2_group);
        yit2_2_le->setObjectName(QString::fromUtf8("yit2_2_le"));
        yit2_2_le->setReadOnly(true);

        gridLayout_4->addWidget(yit2_2_le, 1, 1, 1, 1);


        verticalLayout_4->addWidget(inner_tangent_2_group);


        horizontalLayout_3->addWidget(inner_tangent_group);


        verticalLayout_5->addWidget(tangents_params);


        horizontalLayout->addWidget(sidebar);


        retranslateUi(TangentsTab);
        QObject::connect(x1_box, SIGNAL(valueChanged(double)), gl_widget, SLOT(setX1(double)));
        QObject::connect(x2_box, SIGNAL(valueChanged(double)), gl_widget, SLOT(setX2(double)));
        QObject::connect(r1_box, SIGNAL(valueChanged(double)), gl_widget, SLOT(setR1(double)));
        QObject::connect(y1_box, SIGNAL(valueChanged(double)), gl_widget, SLOT(setY1(double)));
        QObject::connect(y2_box, SIGNAL(valueChanged(double)), gl_widget, SLOT(setY2(double)));
        QObject::connect(r2_box, SIGNAL(valueChanged(double)), gl_widget, SLOT(setR2(double)));
        QObject::connect(gl_widget, SIGNAL(inner_first_tangent_x1_changed(double)), TangentsTab, SLOT(inner_first_tangent_x1_changed(double)));
        QObject::connect(gl_widget, SIGNAL(inner_first_tangent_x2_changed(double)), TangentsTab, SLOT(inner_first_tangent_x2_changed(double)));
        QObject::connect(gl_widget, SIGNAL(inner_first_tangent_y1_changed(double)), TangentsTab, SLOT(inner_first_tangent_y1_changed(double)));
        QObject::connect(gl_widget, SIGNAL(inner_first_tangent_y2_changed(double)), TangentsTab, SLOT(inner_first_tangent_y2_changed(double)));
        QObject::connect(gl_widget, SIGNAL(inner_second_tangent_x1_changed(double)), TangentsTab, SLOT(inner_second_tangent_x1_changed(double)));
        QObject::connect(gl_widget, SIGNAL(inner_second_tangent_x2_changed(double)), TangentsTab, SLOT(inner_second_tangent_x2_changed(double)));
        QObject::connect(gl_widget, SIGNAL(inner_second_tangent_y1_changed(double)), TangentsTab, SLOT(inner_second_tangent_y1_changed(double)));
        QObject::connect(gl_widget, SIGNAL(inner_second_tangent_y2_changed(double)), TangentsTab, SLOT(inner_second_tangent_y2_changed(double)));
        QObject::connect(gl_widget, SIGNAL(out_first_tangent_x1_changed(double)), TangentsTab, SLOT(out_first_tangent_x1_changed(double)));
        QObject::connect(gl_widget, SIGNAL(out_first_tangent_x2_changed(double)), TangentsTab, SLOT(out_first_tangent_x2_changed(double)));
        QObject::connect(gl_widget, SIGNAL(out_first_tangent_y1_changed(double)), TangentsTab, SLOT(out_first_tangent_y1_changed(double)));
        QObject::connect(gl_widget, SIGNAL(out_first_tangent_y2_changed(double)), TangentsTab, SLOT(out_first_tangent_y2_changed(double)));
        QObject::connect(gl_widget, SIGNAL(out_second_tangent_x1_changed(double)), TangentsTab, SLOT(out_second_tangent_x1_changed(double)));
        QObject::connect(gl_widget, SIGNAL(out_second_tangent_x2_changed(double)), TangentsTab, SLOT(out_second_tangent_x2_changed(double)));
        QObject::connect(gl_widget, SIGNAL(out_second_tangent_y1_changed(double)), TangentsTab, SLOT(out_second_tangent_y1_changed(double)));
        QObject::connect(gl_widget, SIGNAL(out_second_tangent_y2_changed(double)), TangentsTab, SLOT(out_second_tangent_y2_changed(double)));

        QMetaObject::connectSlotsByName(TangentsTab);
    } // setupUi

    void retranslateUi(QWidget *TangentsTab)
    {
        TangentsTab->setWindowTitle(QApplication::translate("TangentsTab", "Form", nullptr));
        circle1_group->setTitle(QApplication::translate("TangentsTab", "\320\236\320\272\321\200\321\203\320\266\320\275\320\276\321\201\321\202\321\214 1", nullptr));
        x1_box->setPrefix(QApplication::translate("TangentsTab", "X | ", nullptr));
        y1_box->setPrefix(QApplication::translate("TangentsTab", "Y | ", nullptr));
        r1_box->setPrefix(QApplication::translate("TangentsTab", "R | ", nullptr));
        circle2_group->setTitle(QApplication::translate("TangentsTab", "\320\236\320\272\321\200\321\203\320\266\320\275\320\276\321\201\321\202\321\214 2", nullptr));
        x2_box->setPrefix(QApplication::translate("TangentsTab", "X | ", nullptr));
        y2_box->setPrefix(QApplication::translate("TangentsTab", "Y | ", nullptr));
        r2_box->setPrefix(QApplication::translate("TangentsTab", "R | ", nullptr));
        outer_tangent_group->setTitle(QApplication::translate("TangentsTab", "\320\222\320\275\320\265\321\210\320\275\320\270\320\265 \320\232\320\260\321\201\320\260\321\202\320\265\320\273\321\214\320\275\321\213\320\265", nullptr));
        outer_tangent_1_group->setTitle(QApplication::translate("TangentsTab", "\320\232\320\260\321\201\320\260\321\202\320\265\320\273\321\214\320\275\320\260\321\217 1", nullptr));
        xot1_1_le->setInputMask(QApplication::translate("TangentsTab", "\\X | #9.DDDD", nullptr));
        xot1_2_le->setInputMask(QApplication::translate("TangentsTab", "\\X | #9.DDDD", nullptr));
        yot1_1_le->setInputMask(QApplication::translate("TangentsTab", "\\Y | #9.DDDD", nullptr));
        yot1_2_le->setInputMask(QApplication::translate("TangentsTab", "\\Y | #9.DDDD", nullptr));
        outer_tangent_2_group->setTitle(QApplication::translate("TangentsTab", "\320\232\320\260\321\201\320\260\321\202\320\265\320\273\321\214\320\275\320\260\321\217 2", nullptr));
        xot2_1_le->setInputMask(QApplication::translate("TangentsTab", "\\X | #9.DDDD", nullptr));
        xot2_2_le->setInputMask(QApplication::translate("TangentsTab", "\\X | #9.DDDD", nullptr));
        yot2_1_le->setInputMask(QApplication::translate("TangentsTab", "\\Y | #9.DDDD", nullptr));
        yot2_2_le->setInputMask(QApplication::translate("TangentsTab", "\\Y | #9.DDDD", nullptr));
        inner_tangent_group->setTitle(QApplication::translate("TangentsTab", "\320\222\320\275\321\203\321\202\321\200\320\265\320\275\320\275\320\270\320\265 \320\232\320\260\321\201\320\260\321\202\320\265\320\273\321\214\320\275\321\213\320\265", nullptr));
        inner_tangent_1_group->setTitle(QApplication::translate("TangentsTab", "\320\232\320\260\321\201\320\260\321\202\320\265\320\273\321\214\320\275\320\260\321\217 1", nullptr));
        xit1_1_le->setInputMask(QApplication::translate("TangentsTab", "\\X | #9.DDDD", nullptr));
        xit1_2_le->setInputMask(QApplication::translate("TangentsTab", "\\X | #9.DDDD", nullptr));
        yit1_1_le->setInputMask(QApplication::translate("TangentsTab", "\\Y | #9.DDDD", nullptr));
        yit1_2_le->setInputMask(QApplication::translate("TangentsTab", "\\Y | #9.DDDD", nullptr));
        inner_tangent_2_group->setTitle(QApplication::translate("TangentsTab", "\320\232\320\260\321\201\320\260\321\202\320\265\320\273\321\214\320\275\320\260\321\217 2", nullptr));
        xit2_1_le->setInputMask(QApplication::translate("TangentsTab", "\\X | #9.DDDD", nullptr));
        xit2_2_le->setInputMask(QApplication::translate("TangentsTab", "\\X | #9.DDDD", nullptr));
        yit2_1_le->setInputMask(QApplication::translate("TangentsTab", "\\Y | #9.DDDD", nullptr));
        yit2_2_le->setInputMask(QApplication::translate("TangentsTab", "\\Y | #9.DDDD", nullptr));
    } // retranslateUi

};

namespace Ui {
    class TangentsTab: public Ui_TangentsTab {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TANGENTSTAB_H
