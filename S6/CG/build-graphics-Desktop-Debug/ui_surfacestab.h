/********************************************************************************
** Form generated from reading UI file 'surfacestab.ui'
**
** Created by: Qt User Interface Compiler version 5.12.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SURFACESTAB_H
#define UI_SURFACESTAB_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "glwidgets/surfacesglwidget.h"

QT_BEGIN_NAMESPACE

class Ui_SurfacesTab
{
public:
    QHBoxLayout *horizontalLayout;
    SurfacesGLWidget *gl_widget;
    QWidget *sidebar;
    QVBoxLayout *verticalLayout_3;
    QGroupBox *polyhedron_group;
    QGridLayout *gridLayout_3;
    QTableWidget *polyhydron_table;
    QGroupBox *pointParams;
    QVBoxLayout *verticalLayout_4;
    QDoubleSpinBox *point_y_box;
    QDoubleSpinBox *point_x_box;
    QDoubleSpinBox *point_z_box;
    QWidget *lower_group;
    QGridLayout *gridLayout_4;
    QGroupBox *guidelines_group;
    QVBoxLayout *verticalLayout_2;
    QGroupBox *spline_1_group;
    QGridLayout *gridLayout;
    QLabel *degree_1_label;
    QSpinBox *degree_1_box;
    QGroupBox *spline_2_group;
    QGridLayout *gridLayout_2;
    QLabel *degree_2_label;
    QSpinBox *degree_2_box;
    QGroupBox *rotatation_group;
    QVBoxLayout *verticalLayout;
    QDoubleSpinBox *rotation_x_box;
    QDoubleSpinBox *rotation_y_box;
    QDoubleSpinBox *rotation_z_box;
    QPushButton *redraw_btn;
    QCheckBox *realtime_checkbox;

    void setupUi(QWidget *SurfacesTab)
    {
        if (SurfacesTab->objectName().isEmpty())
            SurfacesTab->setObjectName(QString::fromUtf8("SurfacesTab"));
        SurfacesTab->resize(1096, 718);
        horizontalLayout = new QHBoxLayout(SurfacesTab);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        gl_widget = new SurfacesGLWidget(SurfacesTab);
        gl_widget->setObjectName(QString::fromUtf8("gl_widget"));
        gl_widget->setMinimumSize(QSize(710, 0));

        horizontalLayout->addWidget(gl_widget);

        sidebar = new QWidget(SurfacesTab);
        sidebar->setObjectName(QString::fromUtf8("sidebar"));
        QSizePolicy sizePolicy(QSizePolicy::Maximum, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(sidebar->sizePolicy().hasHeightForWidth());
        sidebar->setSizePolicy(sizePolicy);
        verticalLayout_3 = new QVBoxLayout(sidebar);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        polyhedron_group = new QGroupBox(sidebar);
        polyhedron_group->setObjectName(QString::fromUtf8("polyhedron_group"));
        gridLayout_3 = new QGridLayout(polyhedron_group);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        polyhydron_table = new QTableWidget(polyhedron_group);
        if (polyhydron_table->columnCount() < 10)
            polyhydron_table->setColumnCount(10);
        if (polyhydron_table->rowCount() < 10)
            polyhydron_table->setRowCount(10);
        polyhydron_table->setObjectName(QString::fromUtf8("polyhydron_table"));
        polyhydron_table->setMinimumSize(QSize(340, 0));
        polyhydron_table->setSelectionMode(QAbstractItemView::SingleSelection);
        polyhydron_table->setCornerButtonEnabled(false);
        polyhydron_table->setRowCount(10);
        polyhydron_table->setColumnCount(10);
        polyhydron_table->horizontalHeader()->setDefaultSectionSize(30);

        gridLayout_3->addWidget(polyhydron_table, 1, 0, 1, 1);

        pointParams = new QGroupBox(polyhedron_group);
        pointParams->setObjectName(QString::fromUtf8("pointParams"));
        verticalLayout_4 = new QVBoxLayout(pointParams);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        point_y_box = new QDoubleSpinBox(pointParams);
        point_y_box->setObjectName(QString::fromUtf8("point_y_box"));
        point_y_box->setDecimals(4);
        point_y_box->setMinimum(-999.000000000000000);
        point_y_box->setMaximum(999.000000000000000);
        point_y_box->setSingleStep(0.010000000000000);

        verticalLayout_4->addWidget(point_y_box);

        point_x_box = new QDoubleSpinBox(pointParams);
        point_x_box->setObjectName(QString::fromUtf8("point_x_box"));
        point_x_box->setDecimals(4);
        point_x_box->setMinimum(-999.000000000000000);
        point_x_box->setMaximum(999.000000000000000);
        point_x_box->setSingleStep(0.010000000000000);

        verticalLayout_4->addWidget(point_x_box);

        point_z_box = new QDoubleSpinBox(pointParams);
        point_z_box->setObjectName(QString::fromUtf8("point_z_box"));
        point_z_box->setDecimals(4);
        point_z_box->setMinimum(-999.000000000000000);
        point_z_box->setMaximum(999.000000000000000);
        point_z_box->setSingleStep(0.010000000000000);

        verticalLayout_4->addWidget(point_z_box);


        gridLayout_3->addWidget(pointParams, 0, 0, 1, 1);


        verticalLayout_3->addWidget(polyhedron_group);

        lower_group = new QWidget(sidebar);
        lower_group->setObjectName(QString::fromUtf8("lower_group"));
        gridLayout_4 = new QGridLayout(lower_group);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        guidelines_group = new QGroupBox(lower_group);
        guidelines_group->setObjectName(QString::fromUtf8("guidelines_group"));
        verticalLayout_2 = new QVBoxLayout(guidelines_group);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        spline_1_group = new QGroupBox(guidelines_group);
        spline_1_group->setObjectName(QString::fromUtf8("spline_1_group"));
        gridLayout = new QGridLayout(spline_1_group);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        degree_1_label = new QLabel(spline_1_group);
        degree_1_label->setObjectName(QString::fromUtf8("degree_1_label"));

        gridLayout->addWidget(degree_1_label, 0, 0, 1, 1);

        degree_1_box = new QSpinBox(spline_1_group);
        degree_1_box->setObjectName(QString::fromUtf8("degree_1_box"));
        degree_1_box->setMaximum(3);
        degree_1_box->setValue(2);

        gridLayout->addWidget(degree_1_box, 0, 1, 1, 1);


        verticalLayout_2->addWidget(spline_1_group);

        spline_2_group = new QGroupBox(guidelines_group);
        spline_2_group->setObjectName(QString::fromUtf8("spline_2_group"));
        gridLayout_2 = new QGridLayout(spline_2_group);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        degree_2_label = new QLabel(spline_2_group);
        degree_2_label->setObjectName(QString::fromUtf8("degree_2_label"));

        gridLayout_2->addWidget(degree_2_label, 0, 0, 1, 1);

        degree_2_box = new QSpinBox(spline_2_group);
        degree_2_box->setObjectName(QString::fromUtf8("degree_2_box"));
        degree_2_box->setMaximum(3);
        degree_2_box->setValue(2);

        gridLayout_2->addWidget(degree_2_box, 0, 1, 1, 1);


        verticalLayout_2->addWidget(spline_2_group);


        gridLayout_4->addWidget(guidelines_group, 1, 0, 1, 1);

        rotatation_group = new QGroupBox(lower_group);
        rotatation_group->setObjectName(QString::fromUtf8("rotatation_group"));
        verticalLayout = new QVBoxLayout(rotatation_group);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        rotation_x_box = new QDoubleSpinBox(rotatation_group);
        rotation_x_box->setObjectName(QString::fromUtf8("rotation_x_box"));
        rotation_x_box->setMinimum(-360.000000000000000);
        rotation_x_box->setMaximum(360.000000000000000);
        rotation_x_box->setValue(0.000000000000000);

        verticalLayout->addWidget(rotation_x_box);

        rotation_y_box = new QDoubleSpinBox(rotatation_group);
        rotation_y_box->setObjectName(QString::fromUtf8("rotation_y_box"));
        rotation_y_box->setMinimum(-360.000000000000000);
        rotation_y_box->setMaximum(360.000000000000000);
        rotation_y_box->setValue(0.000000000000000);

        verticalLayout->addWidget(rotation_y_box);

        rotation_z_box = new QDoubleSpinBox(rotatation_group);
        rotation_z_box->setObjectName(QString::fromUtf8("rotation_z_box"));
        rotation_z_box->setMinimum(-360.000000000000000);
        rotation_z_box->setMaximum(360.000000000000000);
        rotation_z_box->setValue(0.000000000000000);

        verticalLayout->addWidget(rotation_z_box);


        gridLayout_4->addWidget(rotatation_group, 1, 1, 1, 1);

        redraw_btn = new QPushButton(lower_group);
        redraw_btn->setObjectName(QString::fromUtf8("redraw_btn"));
        redraw_btn->setEnabled(true);

        gridLayout_4->addWidget(redraw_btn, 2, 1, 1, 1);

        realtime_checkbox = new QCheckBox(lower_group);
        realtime_checkbox->setObjectName(QString::fromUtf8("realtime_checkbox"));

        gridLayout_4->addWidget(realtime_checkbox, 2, 0, 1, 1);


        verticalLayout_3->addWidget(lower_group);


        horizontalLayout->addWidget(sidebar);


        retranslateUi(SurfacesTab);
        QObject::connect(polyhydron_table, SIGNAL(currentCellChanged(int,int,int,int)), SurfacesTab, SLOT(updatePointCoordinates(int,int)));
        QObject::connect(point_x_box, SIGNAL(valueChanged(double)), SurfacesTab, SLOT(updateVertexX(double)));
        QObject::connect(point_y_box, SIGNAL(valueChanged(double)), SurfacesTab, SLOT(updateVertexY(double)));
        QObject::connect(point_z_box, SIGNAL(valueChanged(double)), SurfacesTab, SLOT(updateVertexZ(double)));
        QObject::connect(SurfacesTab, SIGNAL(updateVertexX(double,QPoint)), gl_widget, SLOT(updatePointX(double,QPoint)));
        QObject::connect(SurfacesTab, SIGNAL(updateVertexY(double,QPoint)), gl_widget, SLOT(updatePointY(double,QPoint)));
        QObject::connect(SurfacesTab, SIGNAL(updateVertexZ(double,QPoint)), gl_widget, SLOT(updatePointZ(double,QPoint)));
        QObject::connect(redraw_btn, SIGNAL(clicked()), gl_widget, SLOT(update()));
        QObject::connect(realtime_checkbox, SIGNAL(toggled(bool)), gl_widget, SLOT(toggleRealtimeUpdate(bool)));
        QObject::connect(degree_1_box, SIGNAL(valueChanged(int)), gl_widget, SLOT(setDegreeU(int)));
        QObject::connect(degree_2_box, SIGNAL(valueChanged(int)), gl_widget, SLOT(setDegreeW(int)));
        QObject::connect(rotation_x_box, SIGNAL(valueChanged(double)), gl_widget, SLOT(rotateAroundOX(double)));
        QObject::connect(rotation_y_box, SIGNAL(valueChanged(double)), gl_widget, SLOT(rotateAroundOY(double)));
        QObject::connect(rotation_z_box, SIGNAL(valueChanged(double)), gl_widget, SLOT(rotateAroundOZ(double)));

        QMetaObject::connectSlotsByName(SurfacesTab);
    } // setupUi

    void retranslateUi(QWidget *SurfacesTab)
    {
        SurfacesTab->setWindowTitle(QApplication::translate("SurfacesTab", "Form", nullptr));
        polyhedron_group->setTitle(QApplication::translate("SurfacesTab", "\320\227\320\260\320\264\320\260\321\216\321\211\320\270\320\271 \320\234\320\275\320\276\320\263\320\276\320\263\321\200\320\260\320\275\320\275\320\270\320\272", nullptr));
        pointParams->setTitle(QApplication::translate("SurfacesTab", "\320\222\321\213\320\261\321\200\320\260\320\275\320\275\320\260\321\217 \320\242\320\276\321\207\320\272\320\260", nullptr));
        point_y_box->setPrefix(QApplication::translate("SurfacesTab", "X |  ", nullptr));
        point_x_box->setPrefix(QApplication::translate("SurfacesTab", "Y |  ", nullptr));
        point_z_box->setPrefix(QApplication::translate("SurfacesTab", "Z |  ", nullptr));
        guidelines_group->setTitle(QApplication::translate("SurfacesTab", "\320\235\320\260\320\277\321\200\320\260\320\262\320\273\321\217\321\216\321\211\320\270\320\265", nullptr));
        spline_1_group->setTitle(QApplication::translate("SurfacesTab", "B-\320\241\320\277\320\273\320\260\320\271\320\275 1", nullptr));
        degree_1_label->setText(QApplication::translate("SurfacesTab", "\320\237\320\276\321\200\321\217\320\264\320\276\320\272:", nullptr));
        spline_2_group->setTitle(QApplication::translate("SurfacesTab", "B-\320\241\320\277\320\273\320\260\320\271\320\275 2", nullptr));
        degree_2_label->setText(QApplication::translate("SurfacesTab", "\320\237\320\276\321\200\321\217\320\264\320\276\320\272:", nullptr));
        rotatation_group->setTitle(QApplication::translate("SurfacesTab", "\320\237\320\276\320\262\320\276\321\200\320\276\321\202", nullptr));
        rotation_x_box->setPrefix(QApplication::translate("SurfacesTab", "X |  ", nullptr));
        rotation_x_box->setSuffix(QApplication::translate("SurfacesTab", "\302\260", nullptr));
        rotation_y_box->setPrefix(QApplication::translate("SurfacesTab", "Y |  ", nullptr));
        rotation_y_box->setSuffix(QApplication::translate("SurfacesTab", "\302\260", nullptr));
        rotation_z_box->setPrefix(QApplication::translate("SurfacesTab", "Z |  ", nullptr));
        rotation_z_box->setSuffix(QApplication::translate("SurfacesTab", "\302\260", nullptr));
        redraw_btn->setText(QApplication::translate("SurfacesTab", "\320\237\320\265\321\200\320\265\321\201\321\202\321\200\320\276\320\270\321\202\321\214", nullptr));
        realtime_checkbox->setText(QApplication::translate("SurfacesTab", "\320\222 \321\200\320\265\320\260\320\273\321\214\320\275\320\276\320\274 \320\262\321\200\320\265\320\274\320\265\320\275\320\270", nullptr));
    } // retranslateUi

};

namespace Ui {
    class SurfacesTab: public Ui_SurfacesTab {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SURFACESTAB_H
