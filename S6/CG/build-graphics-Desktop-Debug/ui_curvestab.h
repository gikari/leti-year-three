/********************************************************************************
** Form generated from reading UI file 'curvestab.ui'
**
** Created by: Qt User Interface Compiler version 5.12.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CURVESTAB_H
#define UI_CURVESTAB_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "glwidgets/curvesglwidget.h"

QT_BEGIN_NAMESPACE

class Ui_CurvesTab
{
public:
    QHBoxLayout *horizontalLayout_2;
    CurvesGLWidget *openGLWidget;
    QWidget *sidepanel;
    QVBoxLayout *verticalLayout;
    QGroupBox *point_group;
    QGridLayout *gridLayout;
    QLabel *point_number_label;
    QDoubleSpinBox *y_box;
    QLabel *x_label;
    QComboBox *point_number_box;
    QDoubleSpinBox *x_box;
    QLabel *y_label;
    QGroupBox *spline_group;
    QGridLayout *gridLayout_2;
    QLabel *knot_number_label;
    QLabel *spline_degree_label;
    QSpinBox *spline_degree_box;
    QListWidget *knots_list;
    QComboBox *knot_number_box;
    QLabel *knot_value_label;
    QDoubleSpinBox *knot_value_box;
    QWidget *widget;
    QHBoxLayout *horizontalLayout;
    QPushButton *save_spline_btn;
    QPushButton *clear_splines_btn;

    void setupUi(QWidget *CurvesTab)
    {
        if (CurvesTab->objectName().isEmpty())
            CurvesTab->setObjectName(QString::fromUtf8("CurvesTab"));
        CurvesTab->resize(1141, 888);
        horizontalLayout_2 = new QHBoxLayout(CurvesTab);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        openGLWidget = new CurvesGLWidget(CurvesTab);
        openGLWidget->setObjectName(QString::fromUtf8("openGLWidget"));
        openGLWidget->setMinimumSize(QSize(880, 880));
        openGLWidget->setStyleSheet(QString::fromUtf8("background-color: #ffffff;"));

        horizontalLayout_2->addWidget(openGLWidget);

        sidepanel = new QWidget(CurvesTab);
        sidepanel->setObjectName(QString::fromUtf8("sidepanel"));
        QSizePolicy sizePolicy(QSizePolicy::Maximum, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(sidepanel->sizePolicy().hasHeightForWidth());
        sidepanel->setSizePolicy(sizePolicy);
        verticalLayout = new QVBoxLayout(sidepanel);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        point_group = new QGroupBox(sidepanel);
        point_group->setObjectName(QString::fromUtf8("point_group"));
        gridLayout = new QGridLayout(point_group);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        point_number_label = new QLabel(point_group);
        point_number_label->setObjectName(QString::fromUtf8("point_number_label"));

        gridLayout->addWidget(point_number_label, 0, 0, 1, 1);

        y_box = new QDoubleSpinBox(point_group);
        y_box->setObjectName(QString::fromUtf8("y_box"));
        y_box->setDecimals(4);
        y_box->setMinimum(-99.000000000000000);
        y_box->setSingleStep(0.010000000000000);

        gridLayout->addWidget(y_box, 2, 1, 1, 1);

        x_label = new QLabel(point_group);
        x_label->setObjectName(QString::fromUtf8("x_label"));

        gridLayout->addWidget(x_label, 1, 0, 1, 1);

        point_number_box = new QComboBox(point_group);
        point_number_box->addItem(QString());
        point_number_box->addItem(QString());
        point_number_box->addItem(QString());
        point_number_box->addItem(QString());
        point_number_box->addItem(QString());
        point_number_box->addItem(QString());
        point_number_box->addItem(QString());
        point_number_box->setObjectName(QString::fromUtf8("point_number_box"));
        point_number_box->setEditable(false);

        gridLayout->addWidget(point_number_box, 0, 1, 1, 1);

        x_box = new QDoubleSpinBox(point_group);
        x_box->setObjectName(QString::fromUtf8("x_box"));
        x_box->setEnabled(true);
        x_box->setDecimals(4);
        x_box->setMinimum(-99.000000000000000);
        x_box->setSingleStep(0.010000000000000);

        gridLayout->addWidget(x_box, 1, 1, 1, 1);

        y_label = new QLabel(point_group);
        y_label->setObjectName(QString::fromUtf8("y_label"));

        gridLayout->addWidget(y_label, 2, 0, 1, 1);


        verticalLayout->addWidget(point_group);

        spline_group = new QGroupBox(sidepanel);
        spline_group->setObjectName(QString::fromUtf8("spline_group"));
        gridLayout_2 = new QGridLayout(spline_group);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        knot_number_label = new QLabel(spline_group);
        knot_number_label->setObjectName(QString::fromUtf8("knot_number_label"));

        gridLayout_2->addWidget(knot_number_label, 1, 0, 1, 1);

        spline_degree_label = new QLabel(spline_group);
        spline_degree_label->setObjectName(QString::fromUtf8("spline_degree_label"));

        gridLayout_2->addWidget(spline_degree_label, 0, 0, 1, 1);

        spline_degree_box = new QSpinBox(spline_group);
        spline_degree_box->setObjectName(QString::fromUtf8("spline_degree_box"));
        spline_degree_box->setMinimum(0);
        spline_degree_box->setMaximum(6);
        spline_degree_box->setValue(3);

        gridLayout_2->addWidget(spline_degree_box, 0, 1, 1, 1);

        knots_list = new QListWidget(spline_group);
        knots_list->setObjectName(QString::fromUtf8("knots_list"));

        gridLayout_2->addWidget(knots_list, 3, 0, 1, 2);

        knot_number_box = new QComboBox(spline_group);
        knot_number_box->setObjectName(QString::fromUtf8("knot_number_box"));
        knot_number_box->setEditable(false);

        gridLayout_2->addWidget(knot_number_box, 1, 1, 1, 1);

        knot_value_label = new QLabel(spline_group);
        knot_value_label->setObjectName(QString::fromUtf8("knot_value_label"));

        gridLayout_2->addWidget(knot_value_label, 2, 0, 1, 1);

        knot_value_box = new QDoubleSpinBox(spline_group);
        knot_value_box->setObjectName(QString::fromUtf8("knot_value_box"));
        knot_value_box->setEnabled(true);
        knot_value_box->setDecimals(4);
        knot_value_box->setMinimum(0.000000000000000);
        knot_value_box->setMaximum(1.000000000000000);
        knot_value_box->setSingleStep(0.010000000000000);

        gridLayout_2->addWidget(knot_value_box, 2, 1, 1, 1);


        verticalLayout->addWidget(spline_group);

        widget = new QWidget(sidepanel);
        widget->setObjectName(QString::fromUtf8("widget"));
        horizontalLayout = new QHBoxLayout(widget);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        save_spline_btn = new QPushButton(widget);
        save_spline_btn->setObjectName(QString::fromUtf8("save_spline_btn"));

        horizontalLayout->addWidget(save_spline_btn);

        clear_splines_btn = new QPushButton(widget);
        clear_splines_btn->setObjectName(QString::fromUtf8("clear_splines_btn"));

        horizontalLayout->addWidget(clear_splines_btn);


        verticalLayout->addWidget(widget);


        horizontalLayout_2->addWidget(sidepanel);


        retranslateUi(CurvesTab);
        QObject::connect(point_number_box, SIGNAL(currentIndexChanged(int)), CurvesTab, SLOT(updateBulletsCoordinates(int)));
        QObject::connect(point_number_box, SIGNAL(activated(int)), CurvesTab, SLOT(updateBulletsCoordinates(int)));
        QObject::connect(x_box, SIGNAL(valueChanged(double)), CurvesTab, SLOT(setBulletX(double)));
        QObject::connect(y_box, SIGNAL(valueChanged(double)), CurvesTab, SLOT(setBulletY(double)));
        QObject::connect(spline_degree_box, SIGNAL(valueChanged(int)), openGLWidget, SLOT(setSplineDegree(int)));
        QObject::connect(openGLWidget, SIGNAL(knotsChanged()), CurvesTab, SLOT(updateKnotsList()));
        QObject::connect(knot_number_box, SIGNAL(currentIndexChanged(int)), CurvesTab, SLOT(updateKnotsValue()));
        QObject::connect(knot_value_box, SIGNAL(valueChanged(double)), CurvesTab, SLOT(setKnotValue()));
        QObject::connect(clear_splines_btn, SIGNAL(clicked()), openGLWidget, SLOT(clearSplines()));
        QObject::connect(save_spline_btn, SIGNAL(clicked()), openGLWidget, SLOT(saveSpline()));

        QMetaObject::connectSlotsByName(CurvesTab);
    } // setupUi

    void retranslateUi(QWidget *CurvesTab)
    {
        CurvesTab->setWindowTitle(QApplication::translate("CurvesTab", "Form", nullptr));
        point_group->setTitle(QApplication::translate("CurvesTab", "\320\245\320\260\321\200\320\260\320\272\321\202\320\265\321\200\320\270\321\201\321\202\320\270\320\272\320\270 \320\242\320\276\321\207\320\265\320\272", nullptr));
        point_number_label->setText(QApplication::translate("CurvesTab", "\320\242\320\276\321\207\320\272\320\260:", nullptr));
        x_label->setText(QApplication::translate("CurvesTab", "X:", nullptr));
        point_number_box->setItemText(0, QApplication::translate("CurvesTab", "1", nullptr));
        point_number_box->setItemText(1, QApplication::translate("CurvesTab", "2", nullptr));
        point_number_box->setItemText(2, QApplication::translate("CurvesTab", "3", nullptr));
        point_number_box->setItemText(3, QApplication::translate("CurvesTab", "4", nullptr));
        point_number_box->setItemText(4, QApplication::translate("CurvesTab", "5", nullptr));
        point_number_box->setItemText(5, QApplication::translate("CurvesTab", "6", nullptr));
        point_number_box->setItemText(6, QApplication::translate("CurvesTab", "7", nullptr));

        y_label->setText(QApplication::translate("CurvesTab", "Y:", nullptr));
        spline_group->setTitle(QApplication::translate("CurvesTab", "\320\245\320\260\321\200\320\260\320\272\321\202\320\265\321\200\320\270\321\201\321\202\320\270\320\272\320\270 B-\321\201\320\277\320\273\320\260\320\271\320\275\320\260", nullptr));
        knot_number_label->setText(QApplication::translate("CurvesTab", "\320\243\320\267\320\265\320\273:", nullptr));
        spline_degree_label->setText(QApplication::translate("CurvesTab", "\320\237\320\276\321\200\321\217\320\264\320\276\320\272:", nullptr));
        knot_value_label->setText(QApplication::translate("CurvesTab", "\320\227\320\275\320\260\321\207\320\265\320\275\320\270\320\265:", nullptr));
        save_spline_btn->setText(QApplication::translate("CurvesTab", "\320\227\320\260\320\277\320\276\320\274\320\275\320\270\321\202\321\214", nullptr));
        clear_splines_btn->setText(QApplication::translate("CurvesTab", "\320\236\321\207\320\270\321\201\321\202\320\270\321\202\321\214", nullptr));
    } // retranslateUi

};

namespace Ui {
    class CurvesTab: public Ui_CurvesTab {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CURVESTAB_H
