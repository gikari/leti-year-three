/********************************************************************************
** Form generated from reading UI file 'clippingtab.ui'
**
** Created by: Qt User Interface Compiler version 5.12.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CLIPPINGTAB_H
#define UI_CLIPPINGTAB_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QOpenGLWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ClippingTab
{
public:
    QHBoxLayout *horizontalLayout;
    QOpenGLWidget *gl_widget;
    QWidget *side_panel;
    QVBoxLayout *verticalLayout;
    QGroupBox *group_rectangle;
    QGridLayout *gridLayout_2;
    QTableWidget *table_rectangle;
    QDoubleSpinBox *box_rect_point_value;
    QPushButton *btn_add_rect_point;
    QPushButton *btn_rm_rect_point;
    QGroupBox *group_window;
    QGridLayout *gridLayout;
    QDoubleSpinBox *box_window_point_value;
    QTableWidget *table_window;
    QPushButton *btn_add_window_point;
    QPushButton *btn_rm_window_point;

    void setupUi(QWidget *ClippingTab)
    {
        if (ClippingTab->objectName().isEmpty())
            ClippingTab->setObjectName(QString::fromUtf8("ClippingTab"));
        ClippingTab->resize(878, 608);
        horizontalLayout = new QHBoxLayout(ClippingTab);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        gl_widget = new QOpenGLWidget(ClippingTab);
        gl_widget->setObjectName(QString::fromUtf8("gl_widget"));
        gl_widget->setMinimumSize(QSize(600, 600));

        horizontalLayout->addWidget(gl_widget);

        side_panel = new QWidget(ClippingTab);
        side_panel->setObjectName(QString::fromUtf8("side_panel"));
        side_panel->setMaximumSize(QSize(270, 16777215));
        verticalLayout = new QVBoxLayout(side_panel);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        group_rectangle = new QGroupBox(side_panel);
        group_rectangle->setObjectName(QString::fromUtf8("group_rectangle"));
        gridLayout_2 = new QGridLayout(group_rectangle);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        table_rectangle = new QTableWidget(group_rectangle);
        if (table_rectangle->columnCount() < 2)
            table_rectangle->setColumnCount(2);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        table_rectangle->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        table_rectangle->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        if (table_rectangle->rowCount() < 5)
            table_rectangle->setRowCount(5);
        table_rectangle->setObjectName(QString::fromUtf8("table_rectangle"));
        table_rectangle->setSelectionMode(QAbstractItemView::SingleSelection);
        table_rectangle->setCornerButtonEnabled(false);
        table_rectangle->setRowCount(5);

        gridLayout_2->addWidget(table_rectangle, 2, 0, 1, 2);

        box_rect_point_value = new QDoubleSpinBox(group_rectangle);
        box_rect_point_value->setObjectName(QString::fromUtf8("box_rect_point_value"));

        gridLayout_2->addWidget(box_rect_point_value, 3, 0, 1, 2);

        btn_add_rect_point = new QPushButton(group_rectangle);
        btn_add_rect_point->setObjectName(QString::fromUtf8("btn_add_rect_point"));

        gridLayout_2->addWidget(btn_add_rect_point, 0, 0, 1, 1);

        btn_rm_rect_point = new QPushButton(group_rectangle);
        btn_rm_rect_point->setObjectName(QString::fromUtf8("btn_rm_rect_point"));

        gridLayout_2->addWidget(btn_rm_rect_point, 0, 1, 1, 1);


        verticalLayout->addWidget(group_rectangle);

        group_window = new QGroupBox(side_panel);
        group_window->setObjectName(QString::fromUtf8("group_window"));
        gridLayout = new QGridLayout(group_window);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        box_window_point_value = new QDoubleSpinBox(group_window);
        box_window_point_value->setObjectName(QString::fromUtf8("box_window_point_value"));

        gridLayout->addWidget(box_window_point_value, 3, 0, 1, 2);

        table_window = new QTableWidget(group_window);
        if (table_window->columnCount() < 2)
            table_window->setColumnCount(2);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        table_window->setHorizontalHeaderItem(0, __qtablewidgetitem2);
        QTableWidgetItem *__qtablewidgetitem3 = new QTableWidgetItem();
        table_window->setHorizontalHeaderItem(1, __qtablewidgetitem3);
        if (table_window->rowCount() < 5)
            table_window->setRowCount(5);
        table_window->setObjectName(QString::fromUtf8("table_window"));
        table_window->setSelectionMode(QAbstractItemView::SingleSelection);
        table_window->setCornerButtonEnabled(false);
        table_window->setRowCount(5);

        gridLayout->addWidget(table_window, 2, 0, 1, 2);

        btn_add_window_point = new QPushButton(group_window);
        btn_add_window_point->setObjectName(QString::fromUtf8("btn_add_window_point"));

        gridLayout->addWidget(btn_add_window_point, 0, 0, 1, 1);

        btn_rm_window_point = new QPushButton(group_window);
        btn_rm_window_point->setObjectName(QString::fromUtf8("btn_rm_window_point"));

        gridLayout->addWidget(btn_rm_window_point, 0, 1, 1, 1);


        verticalLayout->addWidget(group_window);


        horizontalLayout->addWidget(side_panel);


        retranslateUi(ClippingTab);
        QObject::connect(btn_add_rect_point, SIGNAL(clicked()), ClippingTab, SLOT(addPointToRectangleTable()));
        QObject::connect(btn_rm_rect_point, SIGNAL(clicked()), ClippingTab, SLOT(removePointFromRectangleTable()));
        QObject::connect(btn_add_window_point, SIGNAL(clicked()), ClippingTab, SLOT(addPointToWindowTable()));
        QObject::connect(btn_rm_window_point, SIGNAL(clicked()), ClippingTab, SLOT(removePointFromWindowTable()));

        QMetaObject::connectSlotsByName(ClippingTab);
    } // setupUi

    void retranslateUi(QWidget *ClippingTab)
    {
        ClippingTab->setWindowTitle(QApplication::translate("ClippingTab", "Form", nullptr));
        group_rectangle->setTitle(QApplication::translate("ClippingTab", "\320\222\320\265\321\200\321\210\320\270\320\275\321\213 \320\234\320\275\320\276\320\263\320\276\321\203\320\263\320\276\320\273\321\214\320\275\320\270\320\272\320\260", nullptr));
        QTableWidgetItem *___qtablewidgetitem = table_rectangle->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QApplication::translate("ClippingTab", "X", nullptr));
        QTableWidgetItem *___qtablewidgetitem1 = table_rectangle->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QApplication::translate("ClippingTab", "Y", nullptr));
        btn_add_rect_point->setText(QApplication::translate("ClippingTab", "\320\224\320\276\320\261\320\260\320\262\320\270\321\202\321\214", nullptr));
        btn_rm_rect_point->setText(QApplication::translate("ClippingTab", "\320\243\320\264\320\260\320\273\320\270\321\202\321\214", nullptr));
        group_window->setTitle(QApplication::translate("ClippingTab", "\320\222\320\265\321\200\321\210\320\270\320\275\321\213 \320\236\320\272\320\275\320\260", nullptr));
        QTableWidgetItem *___qtablewidgetitem2 = table_window->horizontalHeaderItem(0);
        ___qtablewidgetitem2->setText(QApplication::translate("ClippingTab", "X", nullptr));
        QTableWidgetItem *___qtablewidgetitem3 = table_window->horizontalHeaderItem(1);
        ___qtablewidgetitem3->setText(QApplication::translate("ClippingTab", "Y", nullptr));
        btn_add_window_point->setText(QApplication::translate("ClippingTab", "\320\224\320\276\320\261\320\260\320\262\320\270\321\202\321\214", nullptr));
        btn_rm_window_point->setText(QApplication::translate("ClippingTab", "\320\243\320\264\320\260\320\273\320\270\321\202\321\214", nullptr));
    } // retranslateUi

};

namespace Ui {
    class ClippingTab: public Ui_ClippingTab {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CLIPPINGTAB_H
